﻿//    Demon Miner - cryptocurrency miner
//    Copyright (C) 2021  Julian Rodriquez
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.


using System;
using System.Text;
using ILGPU;
using ILGPU.Runtime;
using ILGPU.Runtime.Cuda;
using FrkHash;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Numerics;
using DemonMiner;

namespace DemonMiner.lib
{
    class Miner
    {
        public static MinerSettings mySettings;
        public static long[] CudaMinerThread(int DevIndex, long StartNonce, MinerSettings mySettings,uint SearchSize = 4000000)
        {
            bool found = false;
            long[] rLong = new long[8];
            using var context = Context.CreateDefault();
            var cudaAccelerators = context.GetCudaDevices();
            double aSpeed = 0;
            double passes = 1;
            while (!found)
            {
                using (var accelerator = context.CreateCudaAccelerator(0))
                {
                    // Perform operations
                    
                    System.Diagnostics.Stopwatch consoleUpdateStopwatch = new System.Diagnostics.Stopwatch();

                    var kernel = accelerator.LoadAutoGroupedStreamKernel<Index1D, ArrayView<byte>, ArrayView<long>, long, ArrayView<byte>>(SearchKernel);

                    Workload work = NewWorkload(mySettings);

                    using (var iBuffer =  accelerator.Allocate1D<int>(SearchSize))
                    using (var outputBuffer = accelerator.Allocate1D<long>(3))
                    using (var headerBuffer = accelerator.Allocate1D<byte>(32))
                    using (var difficulyBuffer = accelerator.Allocate1D<byte>(32))
                    {

                        byte[] BlockHead = work.BlockHeader;
                        ulong target = work.Target;

                        consoleUpdateStopwatch.Start();
                        for(int i=0; i< work.BlockHeader.Length; i++)
                        {
                            headerBuffer.MemSet(accelerator.DefaultStream, work.BlockHeader[i],i,1);
                        }

                        for(int i = 0; i <32; i++)
                        {

                            difficulyBuffer.MemSet(accelerator.DefaultStream, 0, i,1);
                        }
                        for (int i = 31; i >= 0; i--)
                        {
                            difficulyBuffer.MemSet(accelerator.DefaultStream, work.PoWBytes[i],i,1);
                            //Console.WriteLine(work.PoWBytes[i]);
                        }
                        
                        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                        sw.Start();
                        kernel((Index1D)iBuffer.Length, (ArrayView<byte>)headerBuffer, (ArrayView<long>)outputBuffer, StartNonce, (ArrayView<byte>)difficulyBuffer);


                        accelerator.Synchronize();
                        sw.Stop();
                        double mSpeed = (iBuffer.Length) / sw.Elapsed.TotalSeconds;
                        string hSpeed = "Mh/s";
                        aSpeed += mSpeed;
                        mSpeed = aSpeed / passes;
                        double speedDiv = 1000000;
                        if (mSpeed < 1000000)
                        {
                            speedDiv = 1000;
                            hSpeed = "Kh/s";
                        }
                        else if (mSpeed > 1000000000 && mSpeed < 1000000000000)
                        {
                            speedDiv = 1000000000;
                            hSpeed = "Gh/s";
                        }
                        else if (mSpeed > 1000000000000)
                        {
                            speedDiv = 1000000000000;
                            hSpeed = "Th/s";
                        }

                        
                        Console.WriteLine("Effective Hash Rate: " + mSpeed / speedDiv + " " + hSpeed + "\n");

                        long[] test = outputBuffer.GetAsArray1D();

                        if (test[0] > 0)
                        {
                            // See if it's REALLY Real;
                            // Get a 64 byte nonce

                            byte[] inonce = (new BigInteger(test[0])).ToByteArray();

                            // Hash concatenation of block header and nonce with Keccak512
                            byte[] ConcatData = ConcatArrays(work.BlockHeader, inonce);

                            // Seed hash 
                            byte[] seedHash = Ketchup.Hash(ConcatData, 512);

                            // Hash the results from the first hash using Keccak256
                            byte[] hash = Ketchup.Hash(seedHash, 256);

                            // Verify the hash and send solution
                            if (new BigInteger(hash, false, true) < work.ProofOfWorkDifficulty)
                            {
                                
                                // Convert nonce to hex number
                                string nonceString = (new BigInteger(inonce).ToString("x16"));
                                // Get the mix hash.

                                string mix = ConvertBytesToStringHash(seedHash).Substring(64);
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(
                                    new MediaTypeWithQualityHeaderValue("application/json"));
                                client.DefaultRequestHeaders.Add("User-Agent", "Demon Miner v0.0.1");

                                StringContent SendContent = new StringContent("{ \"jsonrpc\":\"2.0\", \"method\":\"eth_submitWork\", \"params\":[\"0x" + nonceString + "\",\"" + work.BlockHeaderString + "\", \"0x" + mix + "\"    ], \"id\":73 }", Encoding.UTF8, "application/json");

                                var stringTask = client.PostAsync(mySettings.serverAddress, SendContent);

                                string msg = stringTask.Result.Content.ReadAsStringAsync().Result;

                                Response response = JsonSerializer.Deserialize<Response>(msg);
                                if(response.result)
                                {
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    Console.WriteLine("Share Accepted!");
                                    Console.ResetColor();
                                } else
                                {
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Share Rejected!");
                                    Console.ResetColor();
                                }
                            }
                            else
                            {
                                // This shouldn't happen.
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Invalid Hash!");
                                Console.ResetColor();
                            }
                        }
                        passes++;
                        StartNonce = (long)(new BigInteger(Workload.CalculateNonce()));
                        accelerator.Dispose();
                        outputBuffer.Dispose();
                        headerBuffer.Dispose();
                        difficulyBuffer.Dispose();
                        iBuffer.Dispose();

                    }
                }
                
            }
            return rLong;
        }

        public class Response
        {
            public string jsonrpc { get; set; }
            public int id { get; set; }
            public bool result { get; set; }
        }
        private static readonly HttpClient client = new HttpClient();
        private static Workload NewWorkload(MinerSettings mySettings)
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("User-Agent", "Demon Miner v0.0.1");

            var stringTask = client.PostAsync(mySettings.serverAddress, new StringContent("{ \"jsonrpc\":\"2.0\", \"method\":\"eth_getWork\", \"params\":[], \"id\":73 }", Encoding.UTF8, "application/json"));

            string msg = stringTask.Result.Content.ReadAsStringAsync().Result;
            
            return ParseWorkload(msg);
        }

        private static Workload ParseWorkload(string content)
        {

            GetworkResponse ResponseContent = JsonSerializer.Deserialize<GetworkResponse>(content);
            if (ResponseContent.result.Length >= 3)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("Receive new work. Header: ");
                Console.ResetColor();
                Console.Write(ResponseContent.result[0]);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("\nDifficulty: ");
                Console.ResetColor();
                Console.Write(ResponseContent.result[2] + "\n");
                return new Workload(ResponseContent.result[0], ResponseContent.result[2]);
            }

            return null;
        }

        public static byte[] ConcatArrays(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        private static void SearchKernel(Index1D idex, ArrayView<byte> header, ArrayView<long> output, long startNonce, ArrayView<byte> target)
        {
            if (output[0] > 0) return;
            byte[] seeds = new byte[40];
            byte[] sed = new byte[8];
            byte[] seedhash = new byte[64];
            byte[] final = new byte[32];
            long myNonce;
            myNonce = ((long)(ulong)idex + startNonce);

            sed[7] = (byte)(myNonce);
            sed[6] = (byte)(myNonce >> 8);
            sed[5] = (byte)(myNonce >> 16);
            sed[4] = (byte)(myNonce >> 24);
            sed[3] = (byte)(myNonce >> 32);
            sed[2] = (byte)(myNonce >> 40);
            sed[1] = (byte)(myNonce >> 48);
            sed[0] = (byte)(myNonce >> 56);
            int zeros = 0;
            while (sed[zeros] == 00 && zeros < 8)
            {
                zeros++;
            }

            for (int i = 0; i < (8 - zeros); i++)
            {
                seeds[i + 32] = sed[7 - i];
            }

            for (long i = 0; i < 32; i++)
            {

                seeds[i] = header[i];

            }
            seedhash = Algorithms.Optimized_Hash(seeds, 512, 40 - zeros);
            final = Algorithms.Optimized_Hash(seedhash, 256);

            bool isGood = false;
            for(int i=0;i<32;i++)
            {
                if(target[i] > final[i])
                {
                    isGood = true;
                    output[1] = target[i];
                    output[2] = final[i];
                     break;
                } else if(target[i] < final[i])
                {
                    isGood = false;
                    break;
                }
            }
            
            if (isGood) {
                output[0] = myNonce;
            };
        }
        public static string ConvertBytesToStringHash(byte[] hashBytes)
        {
            return BitConverter.ToString(hashBytes).Replace("-", string.Empty).ToLower();
        }
    }
}

