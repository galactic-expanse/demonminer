﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemonMiner.lib
{
    static class Help
    {

        public static void printManual()
        {
            // This is the full help menu.

            Console.Write("" +
                "Demon Miner Commands\n" + 
                "--------------------\n\n" +
                "-S [Server Address]\tSets the address the miner will be connecting to (Example: -S 127.0.0.1)\n" +
                "-P [Port]          \tSets the port for the server connection.\n" +
                "-C [Threads]       \tEnable CPU Mining on set number of threads. Use \'All\' to use all available threading.  (Example: -C 4 or -C All)\n" +
                "-U [Devices]       \tEnable Cuda Mining on device IDs.  Use \'All\' to use all available devices (Example: -U 1,2,3 or -U All)\n" +
                "-O [Devices]       \tEnable OpenCL mining on device IDs.  Use \'All\' to use all available devices (Example: -O 1,2,3 or -O All)\n" +
                "-I [User ID]       \tSets the user ID for connection string.  Refer to your pool for this value.\n" +
                "-W [Worker Name]   \tSets worker name for pools that support it.\n" +
                "-P [Password]      \tSets password for pools that support it.\n" +
                "-R [Retries]       \tNumber of retries to attempt if connection fails. (default: 3)\n" +
                "-T [Timeout]       \tNetwork timeout in seconds (default: 120)\n" +
                "-M [Protocol]      \tConnection protocol (getwork, stratum, stratum1, stratum2)\n" +
                "-? -help           \tDisplay this menu.\n\n");
                
            System.Environment.Exit(0);
        }
    }
}
